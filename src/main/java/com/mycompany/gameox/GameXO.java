/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.gameox;

/**
 *
 * @author commis
 */
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class GameXO {
static ArrayList<Integer> player1 = new ArrayList<Integer>();
	static ArrayList<Integer> player2 = new ArrayList<Integer>();

	public static void main(String[] args) {

		System.out.println("Welcome to OX Game");
		System.out.println();
		char[][] GameOX = { { '1', '-', ' ', '-', ' ', '-' }, { '2', '-', ' ', '-', ' ', '-' },
				{ '3', '-', ' ', '-', ' ', '-' } };
		Game(GameOX);

		String win;
		while (true) {
			Scanner kb = new Scanner(System.in);
			System.out.println("X turn");
			System.out.println("please input Row Col: ");
			int position1 = kb.nextInt();
			place1(GameOX, position1, "play1");
			Game(GameOX);
			win = CheckWiner();
			if (win.equals("X")) {
				System.out.println("Player X Win...");
				System.out.println("Bye bye...");
				break;
			} else if (win.equals("O")) {
				System.out.println("Player O Win...");
				System.out.println("Bye bye...");
				break;
			}

			System.out.println("O turn");
			System.out.println("please input Row Col: ");
			int position2 = kb.nextInt();
			place2(GameOX, position2, "play2");
			Game(GameOX);
			win = CheckWiner();
			if (win.equals("X")) {
				System.out.println("Player X Win...");
				System.out.println("Bye bye...");
				break;
			} else if (win.equals("O")) {
				System.out.println("Player O Win...");
				System.out.println("Bye bye...");
				break;
			}
		}

	}

	public static void Game(char[][] GameOX) {
		System.out.println(" 1 2 3");
		for (char[] row : GameOX) {
			for (char c : row) {
				System.out.print(c);
			}
			System.out.println();
		}
	}

	public static void place1(char[][] GameOX, int position1, String user) {
		char turn = ' ';// smbol
		if (user.equals("play1")) {
			turn = 'X';
			player1.add(position1);
		} else if (user.equals("play2")) {
			turn = 'O';
			player1.add(position1);
		}
		switch (position1) {
		case 1:
			GameOX[0][1] = turn;
			break;
		case 2:
			GameOX[0][3] = turn;
			break;
		case 3:
			GameOX[0][5] = turn;
			break;
		case 4:
			GameOX[1][1] = turn;
			break;
		case 5:
			GameOX[1][3] = turn;
			break;
		case 6:
			GameOX[1][5] = turn;
			break;
		case 7:
			GameOX[2][1] = turn;
			break;
		case 8:
			GameOX[2][3] = turn;
			break;
		case 9:
			GameOX[2][5] = turn;
			break;
		default:
			break;
		}
	}

	public static void place2(char[][] GameOX, int position2, String user) {
		char turn = ' ';// smbol
		if (user.equals("play1")) {
			turn = 'X';
			player2.add(position2);
		} else if (user.equals("play2")) {
			turn = 'O';
			player2.add(position2);
		}
		switch (position2) {
		case 1:
			GameOX[0][1] = turn;
			break;
		case 2:
			GameOX[0][3] = turn;
			break;
		case 3:
			GameOX[0][5] = turn;
			break;
		case 4:
			GameOX[1][1] = turn;
			break;
		case 5:
			GameOX[1][3] = turn;
			break;
		case 6:
			GameOX[1][5] = turn;
			break;
		case 7:
			GameOX[2][1] = turn;
			break;
		case 8:
			GameOX[2][3] = turn;
			break;
		case 9:
			GameOX[2][5] = turn;
			break;
		default:
			break;
		}
	}

	public static String CheckWiner() {
		List topRow = Arrays.asList(1, 2, 3);
		List midRow = Arrays.asList(4, 5, 6);
		List botRow = Arrays.asList(7, 8, 9);
		List lefcol = Arrays.asList(1, 4, 7);
		List midcol = Arrays.asList(2, 5, 8);
		List righcol = Arrays.asList(3, 6, 9);
		List cross1 = Arrays.asList(1, 5, 9);
		List cross2 = Arrays.asList(7, 5, 3);

		List<List> winning = new ArrayList<List>();
		winning.add(topRow);
		winning.add(midRow);
		winning.add(botRow);
		winning.add(lefcol);
		winning.add(midcol);
		winning.add(righcol);
		winning.add(cross1);
		winning.add(cross2);

		for (List w : winning) {
			if (player1.containsAll(w)) {
				return "X";
			} else if (player2.containsAll(w)) {
				return "O";
			}
		}
		return " ";
	}
}
